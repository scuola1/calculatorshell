#!/bin/bash

clear
echo -e '\033[1;31m
 _           _ _       _                 _      
| |         | | |     ( )               | |     
| |     ___ | | | ___ |/ ___    ___ __ _| | ___ 
| |    / _ \| | |/ _ \  / __|  / __/ _` | |/ __|
| |___| (_) | | | (_) | \__ \ | (_| (_| | | (__ 
\_____/\___/|_|_|\___/  |___/  \___\__,_|_|\___|
                                                
\033[0m                                                '


#######################################
#         Common functions
#######################################

#######################################
# This function show the results (first input param) and save it if the second input param is "y"
# ex of usage:
# printResult "0-9" "y/n"
##########################################
function printResult(){
   local partialResult="Il risultato è: $1"
   local isToSave="$2"
   # save only if isToSave at "y"
   if [ $isToSave == "y" ]
   then
      echo "$partialResult" > ./risultato.txt
      echo "Risultato salvato nel file"
   fi
   # pritn result to command line
   echo "$partialResult"
}

#######################################
#         Program initialization    
#######################################

# ask to the user if the program must save the result in a file risultato.txt
# i decided to not save default value beacuse it isn't important
currentPath=$(pwd)
echo -e "Vuoi salvare il risultato parziale/finale nel file '$currentPath/risultato.txt', il valore iniziale non verrà salvato  \033[33m(y/n)\033[0m: "
read savePartialResult
# read one numbers and assigned them to var
echo -e 'Dimmi un valore iniziale \033[33m(0-9)\033[0m: '
read result

# display back the number - punched by user. 
echo "Il risultato parziale è: $result"

# set default value to "y" for force to start first operation without asking anything
newOp="y"

#######################################
#           Manage operation          
#######################################

while [ $newOp == "y" ]
do
   # choose operation
   echo -e "Digitare il numero corrispodente alla operazione che si vuole eseguire \033[33m(1-4)\033[0m: \033[36m$result (+ - * /) x = parziale\033[0m
   \033[33m1\033[0m.Addizione
   \033[33m2\033[0m.Sottrazione
   \033[33m3\033[0m.Moltiplicazione
   \033[33m4\033[0m.Divisione"
   read sceltaOperazione

   # value to add at the first insert value 'result' for the choosen operation
   echo -e "Digitare il numero da utilizzare per l'operazione \033[33m(0-9)\033[0m: "
   read value

   clear

   #######################################
   #           Check Condition
   #######################################
   # switch case for check the value of the variable 'value' 
   case $sceltaOperazione in
      "1")
         # addition
         echo -e "Hai scelto addizione: \033[36m$result + $value =\033[0m "
         result=$(( result + value ))
         ;;
      "2")
         # substration
         echo -e "Hai scelto sottrazione: \033[36m$result - $value =\033[0m "
         result=$(( result - value ))
         ;;
      "3")
         # multply
         echo -e "Hai scelto moltiplicazione: \033[36m$result * $value =\033[0m "
         result=$(( result * value ))
         ;;
      "4")
         # divide
         echo -e "Hai scelto divisione: \033[36m$result / $value =\033[0m "
         # check if the variable 'value' is = 0
         if [ $value == 0 ]
         then
            echo "La divisione non si può fare, perché il denominatore passato è 0, questa operazione non verrà presa in considerazione"
         # value != 0
         else
            result=$(( result / value ))
         fi
         ;;
      *)
         # if the variable 'result' is a different value from 1-2-3-4, this case print to screen 'Operazione non consentita'
         echo "Operazione non consentita"
         ;;
   esac

   #######################################
   #           Show Partial Result
   #######################################
   # print the value of 'result' on screen
   echo -e "\033[36m$result\033[0m (nuovo parziale)"

   # if the user answered 'y' to the question 'Do you want to save the result in a file risultato.txt?' the variable 'result' will print on a file of text
   printResult "$result" "$savePartialResult"

   # if the user set the value to "y" the cicle continue, if the user set the value to "n/anything" the cicle stop
   echo -e "Vuoi continuare con una nuova operazione \033[33m(y/n)\033[0m: "
   read newOp
   clear
done

# when the user answered 'no' to the 'Do you continue?' the result always will save on the file of text 'risultato.txt'
printResult "$result" "y"
